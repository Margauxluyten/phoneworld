package com.margaux2.domain;

public class Phone {
     public String model;
     public String brand;


    public Phone(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public void getInfo() {

        System.out.println("There is have an "+brand+ " in our system");
    }

    public String getBrand() {

        return brand;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {

        this.model = model;
    }

}
