package com.margaux2.services;

import com.margaux2.domain.Phone;

import java.util.ArrayList;
import java.util.Scanner;



public class ChoosePhoneWorld {
    static  Scanner scanner = new Scanner(System.in);
    static ArrayList<Phone> phones = new ArrayList<>();


    public static void showOptionsPhoneWorld() {
        System.out.println("Please select an option :");
        System.out.println("\t1. Let us know what kind of phone you have !");
        System.out.println("\t2. Are you happy with your phone ?");
        System.out.println("\t3. Buy new phone from available options in our store .");
        System.out.println("\t4. Show already given phones by the people who came to visit out store .");
        System.out.println("\t5. Delete Phone .");
        System.out.println("\t6. Search phones .");


        int option = scanner.nextInt();

        switch (option) {
            case 1:
                addPhone();
                break;
            case 2:
                happyWithPhone();
                break;
            case 3:
                buyNewPhone();
                break;
            case 4:
                showPhone();
                break;
            case 5:
                deletePhone();
                break;
            case 6:
                searchPhone();
                break;
            default:
                System.out.println("No valid input");
                System.out.println("Back to main menu");
                showOptionsPhoneWorld();

        }

    }

    private static void deletePhone() {
        System.out.println("Please enter the Phone you wish to delete .");
        String namePhone = scanner.next();
        if(namePhone.equals("")){
            System.out.println("please enter valid input .");
            deletePhone();
        }else{
            boolean doesExist = false;
            for(Phone ph:phones){
                if(ph.getBrand().equals(namePhone)){
                    doesExist= true;
                    phones.remove(ph);

                }
            }
            if(!doesExist){
                System.out.println("No phone like that one .");
            }

        }
        showOptionsPhoneWorld();


    }

    private static void showPhone(){
        for(Phone ph : phones){
            ph.getInfo();
            System.out.println("-------------------------------------");

        }showOptionsPhoneWorld();

    }

    private static void addPhone() {
        System.out.println("What is the brand of your Phone ?");
        String brand = scanner.next();
        System.out.println("What is the model of your " + brand + " ?");
        scanner.nextLine();
        String model = scanner.nextLine();
        System.out.println("Your Phone is " + brand + " " + model);
        Phone phone = new Phone(brand, model);
        phones.add(phone);
        showOptionsPhoneWorld();
    }



    private static void happyWithPhone() {
        System.out.println("Are you happy with your Phone please give it a rating for 0-10 ?");
        int rating = scanner.nextInt();
        if (rating <= 5) {
            System.out.println("Really unhappy! Time for a new phone !");
            buyNewPhone();
        } else if (rating <= 7) {
            System.out.println("This Phone is good but maybe you want a new one ? ");
            buyNewPhone();
        } else if (rating <=10) {
            System.out.println("Excellent Phone");
        } else if(rating >10){
            System.out.println("Invalid input, back to main menu");

        } else{

        }
        showOptionsPhoneWorld();

    }

    private static void buyNewPhone() {
        System.out.println("Want to buy a now Phone ?");

        showOptionsPhoneWorld();

    }

    private static void searchPhone() {
        System.out.println("Look if we have clients with the same phone as you.");
        System.out.println("Please enter the Phone ?");
        String namePhone = scanner.next();
        if(namePhone.equals("")){
            System.out.println("Please enter valid Phone");
            searchPhone();
        }else{
            boolean doesExist =false;
            for(Phone p: phones){
                if(p.getBrand().equals(namePhone)){
                    doesExist = true;
                    p.getInfo();
                }
            }
            if (!doesExist){
                System.out.println("Phone not found");
            }
        }
        showOptionsPhoneWorld();

    }


}
